<?php


namespace Tests\AppBundle\Entity;

use AppBundle\Entity\BaseMaintenance;
use PHPUnit\Framework\TestCase;


class BaseMaintenanceTest extends TestCase
{
    public function testDefaults()
    {
        $task = new BaseMaintenance();
        self::assertEquals(0, $task->getCost());
        self::assertEquals(new \DateTime(), $task->getDate());
    }

    public function testSetters()
    {
        $task = new BaseMaintenance();
        $task->setCost(32.4);
        self::assertEquals(32.4, $task->getCost());
        $task->setCost(-3);
        self::assertEquals(32.4, $task->getCost());
        $now = new \DateTime();
        $task->setDate($now);
        self::assertEquals($now, $task->getDate());
        $invalidDate = \DateTime::createFromFormat('d-m-Y', 'invalidstring');
        $task->setDate($invalidDate);
        self::assertEquals($now, $task->getDate());
    }
}