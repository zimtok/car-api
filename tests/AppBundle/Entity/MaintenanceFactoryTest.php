<?php


namespace Tests\AppBundle\Entity;
use AppBundle\Entity\BaseMaintenance;
use AppBundle\Entity\BrakeRefill;
use AppBundle\Entity\MaintenanceFactory;
use AppBundle\Entity\OilChange;
use AppBundle\Entity\TireRotation;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class MaintenanceFactoryTest extends TestCase
{
    public function testOilCreation(){
        $mock_request = $this->getMock(Request::class);
        $mock_request->method('get')
            ->willReturn('oil_change');
        $task = MaintenanceFactory::createFromRequest($mock_request);
        self::assertInstanceOf(OilChange::class, $task);
    }

    public function testTireCreation()
    {
        $mock_request = $this->getMock(Request::class);
        $mock_request->method('get')
            ->willReturn('tire_rotation');
        $task = MaintenanceFactory::createFromRequest($mock_request);
        self::assertInstanceOf(TireRotation::class, $task);
    }

    public function testBrakeCreation()
    {
        $mock_request = $this->getMock(Request::class);
        $mock_request->method('get')
            ->willReturn('brake_refill');
        $task = MaintenanceFactory::createFromRequest($mock_request);
        self::assertInstanceOf(BrakeRefill::class, $task);
    }

    public function testUnknownType()
    {
        $mock_request = $this->getMock(Request::class);
        $mock_request->method('get')
            ->willReturn('unknown');
        $this->setExpectedException(\UnexpectedValueException::class);
        $task = MaintenanceFactory::createFromRequest($mock_request);

    }

    public function testSetParameters()
    {
        $task = new BaseMaintenance();
        $date = '28-11-2017';
        $mock_request = $this->getMock(Request::class);
        $mock_request->expects(self::any())
            ->method('get')
            ->withConsecutive(['cost'], ['date'])
            ->willReturnOnConsecutiveCalls(42.5, $date);
        MaintenanceFactory::setParameters($task, $mock_request);
        self::assertEquals(42.5, $task->getCost());
        self::assertInstanceOf(\DateTime::class, $task->getDate());
    }
}