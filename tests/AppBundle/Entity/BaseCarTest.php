<?php

namespace Tests\AppBundle\Entity; 

use AppBundle\Entity\BaseCar;
use AppBundle\Entity\BaseMaintenance;
use PHPUnit\Framework\TestCase;

class BaseCarTest extends TestCase
{
    public function testDefaults()
    {
        $car = new BaseCar();
        self::assertEquals(1900, $car->getYear());
        self::assertEquals(0, $car->getOdometer());
        self::assertEquals("N/A", $car->getMake());
        self::assertEquals("N/A", $car->getModel());
        self::assertEquals(0, $car->getMaintenanceTasks()->count());
    }

    public function testSetters()
    {
        $car = new BaseCar();
        $car->setMake("make");
        self::assertEquals("make", $car->getMake());
        $car->setMake(null);
        self::assertEquals("make", $car->getMake());
        $car->setModel("model");
        self::assertEquals("model", $car->getModel());
        $car->setModel(null);
        self::assertEquals("model", $car->getModel());
        $car->setOdometer(42);
        self::assertEquals(42, $car->getOdometer());
        $car->setOdometer(-20);
        self::assertEquals(42, $car->getOdometer());
        $car->setYear(2017);
        self::assertEquals(2017, $car->getYear());
        $car->setYear(1061);
        self::assertEquals(2017, $car->getYear());
    }

    public function testAddTask()
    {
        $car = new BaseCar();
        $mockTask = $this->getMock(BaseMaintenance::class);
        $car->addMaintenanceTask($mockTask);
        self::assertEquals(1, $car->getMaintenanceTasks()->count());
    }

}