<?php


namespace Tests\AppBundle\Entity;

use AppBundle\Entity\ElectricCar;
use AppBundle\Entity\OilChange;

use PHPUnit\Framework\TestCase;

class ElectricCarTest extends TestCase
{
    public function testInvalidTask()
    {
        $car = new ElectricCar();
        $mock_task = $this->getMock(OilChange::class);
        $mock_task->method('getMaintenanceType')
            ->willReturn('Oil change');
        $this->setExpectedException(\UnexpectedValueException::class);
        $car->addMaintenanceTask($mock_task);
    }
}