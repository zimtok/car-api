<?php


namespace Tests\AppBundle\Entity;

use AppBundle\Entity\BaseCar;
use AppBundle\Entity\DieselCar;
use AppBundle\Entity\ElectricCar;
use AppBundle\Entity\GasCar;
use PHPUnit\Framework\TestCase;
use AppBundle\Entity\CarFactory;
use Symfony\Component\HttpFoundation\Request;


class CarFactoryTest extends TestCase
{
    public function testElectricCreation()
    {
        $mockRequest = $this->getMock(Request::class);
        $mockRequest->method('get')
            ->willReturn('electric');
        $car = CarFactory::createFromRequest($mockRequest);
        self::assertInstanceOf(ElectricCar::class, $car);
    }

    public function testDieselCreation()
    {
        $mockRequest = $this->getMock(Request::class);
        $mockRequest->method('get')
            ->willReturn('diesel');
        $car = CarFactory::createFromRequest($mockRequest);
        self::assertInstanceOf(DieselCar::class, $car);
    }

    public function testGasCreation()
    {
        $mockRequest = $this->getMock(Request::class);
        $mockRequest->method('get')
            ->willReturn('gas');
        $car = CarFactory::createFromRequest($mockRequest);
        self::assertInstanceOf(GasCar::class, $car);
    }

    public function testUnknownType()
    {
        $mockRequest = $this->getMock(Request::class);
        $mockRequest->method('get')
            ->willReturn('unknown');
        $this->setExpectedException(\UnexpectedValueException::class);
        $car = CarFactory::createFromRequest($mockRequest);

    }

    public function testSetParameters()
    {
        $mockRequest = $this->getMock(Request::class);
        $mockRequest->expects(self::any())->method('get')
            ->withConsecutive(['odo'], ['year'], ['make'], ['model'])
            ->willReturnOnConsecutiveCalls(100, 2000, 'make', 'model');
        $car = new BaseCar();
        CarFactory::setParameters($car, $mockRequest);
        self::assertEquals(100, $car->getOdometer());
        self::assertEquals(2000, $car->getYear());
        self::assertEquals('make', $car->getMake());
        self::assertEquals('model', $car->getModel());

    }
}