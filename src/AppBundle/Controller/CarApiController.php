<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BaseCar;
use AppBundle\Entity\BaseMaintenance;
use AppBundle\Entity\CarFactory;
use AppBundle\Entity\MaintenanceFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CarApiController extends FOSRestController
{

    /**
     * @Route("/api/{slug}", defaults={"slug" = null}, requirements={"slug"=".+"})
     * @Method("OPTIONS")
     */
    public function optionsAction($slug)
    {
        return $this->createResponse(null);
    }

    /**
     * @Route("/api")
     * @Method({"GET", "HEAD"})
     *
     * List all the cars
     *
     *
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository('AppBundle:BaseCar')->findAll();
        $result_list = array();
        foreach ($result as $item){
            $result_list[] = $item->toArray();
        }
        $ret = $this->createResponse($result_list);
        return $ret;
    }

    /**
     * @Route("/api/{id}")
     * @Method({"GET", "HEAD"})
     *
     * Return the car with the specified ID.
     * Returns with 404 if the ID doesn't exist
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository("AppBundle:BaseCar")->find($id);
        if($result == null){
            return $this->createNotFoundResponse();
        }
        $ret = $this->createResponse($result->toArray());
        return $ret;
    }

    /**
     * @Route("/api")
     * @Method("POST")
     *
     * Create a new car
     * The request body must at least have a 'type' parameter
     *
     */
    public function postAction(Request $request)
    {
        try {
            $car = CarFactory::createFromRequest($request);
            $em = $this->getDoctrine()->getManager();
            $em->persist($car);
            $em->flush();
            return $this->createResponse($car->toArray());
        } catch (\UnexpectedValueException $exception){
            return $this->handleError($exception);
        }
    }

    /**
     * @Route("/api/{id}")
     * @Method("PUT")
     *
     * Edit the car identified by ID or create a new one if it doesn't exist.
     * The response has status code 200 if a car was edited, and 201 if a new one was created
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $car = $em->getRepository('AppBundle:BaseCar')->find($id);
        $statusCode = Response::HTTP_OK;
        if($car == null){
            $car = CarFactory::createFromRequest($request);
            $statusCode = Response::HTTP_CREATED;
        }else{
            CarFactory::setParameters($car, $request);
        }
        $em->persist($car);
        $em->flush();
        $ret = $this->createResponse($car->toArray());
        $ret->setStatusCode($statusCode);
        return $ret;
    }

    /**
     * @Route("/api/{id}")
     * @Method("DELETE")
     *
     * Delete the car identified by ID.
     * The response has status code 204 if a delete took place and 404 if no car was found
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository('AppBundle:BaseCar')->find($id);

        if($result != null){
            $em->remove($result);
            $em->flush();
            $ret = $this->createResponse(null);
            $ret->setStatusCode(Response::HTTP_NO_CONTENT);
            return $ret;
        }else{
            return $this->createNotFoundResponse();
        }

    }


    /**
     * @Route("/api/{id}/task")
     * @Method({"GET", "HEAD"})
     *
     * List all the tasks associated with one car
     */
    public function listTasksAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository("AppBundle:BaseCar")->find($id);
        if($result == null){
            return $this->createNotFoundResponse();
        }
        $result_list = $result->toArray()["maintenance_tasks"];
        return $this->createResponse($result_list);

    }

    /**
     * @Route("/api/{car_id}/task/{task_id}")
     * @Method({"GET", "HEAD"})
     *
     * Show a specific task associated with a car
     */
    public function showTaskAction($car_id, $task_id)
    {
        $em = $this->getDoctrine()->getManager();
        $car_result = $em->getRepository("AppBundle:BaseCar")->find($car_id);
        $task_result = $em->getRepository("AppBundle:BaseMaintenance")->find($task_id);
        if($car_result == null || $task_result == null || $task_result->getCar() != $car_result){
            return $this->createNotFoundResponse();
        }
        $ret = $this->createResponse($task_result->toArray());
        return $ret;
    }


    /**
     * @Route("/api/{id}/task")
     * @Method("POST")
     *
     * Add a new task to the car identifed by ID
     * The request must at lest have a 'type' parameter
     */
    public function createTaskAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $car = $em->getRepository('AppBundle:BaseCar')->find($id);
        if($car == null){
            return $this->createNotFoundResponse();
        }
        try {
            $task = MaintenanceFactory::createFromRequest($request);
            $car->addMaintenanceTask($task);
            $em->persist($car);
            $em->flush();
            return $this->createResponse($task->toArray());
        } catch (\UnexpectedValueException $exception){
            return $this->handleError($exception);
        }
    }

    /**
     * @Route("/api/{car_id}/task/{task_id}")
     * @Method("PUT")
     *
     * Edit an existing task or create a new one
     */
    public function editTaskAction($car_id, $task_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /**
         * @var BaseCar $car_result
         */
        $car_result = $em->getRepository("AppBundle:BaseCar")->find($car_id);
        /**
         * @var BaseMaintenance $task_result
         */
        $task_result = $em->getRepository("AppBundle:BaseMaintenance")->find($task_id);
        $statusCode = Response::HTTP_OK;
        if($car_result == null || ($task_result != null && $task_result->getCar() != $car_result)){
            return $this->createNotFoundResponse();
        }else if($task_result != null){
            MaintenanceFactory::setParameters($task_result, $request);
            $em->persist($task_result);
        }else{
            $task_result = MaintenanceFactory::createFromRequest($request);
            $car_result->addMaintenanceTask($task_result);
            $em->persist($car_result);
            $statusCode = Response::HTTP_CREATED;
        }
        $em->flush();
        $ret = $this->createResponse($task_result->toArray());
        $ret->setStatusCode($statusCode);
        return $ret;
    }

    /**
     * @Route("/api/task/{task_id}")
     * @Method("DELETE")
     *
     * Delete a task
     */
    public function deleteTaskAction($task_id)
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository('AppBundle:BaseMaintenance')->find($task_id);
        if($task != null){
            $em->remove($task);
            $em->flush();
            $ret = $this->createResponse(null);
            $ret->setStatusCode(Response::HTTP_NO_CONTENT);
            return $ret;
        }else {
            return $this->createNotFoundResponse();
        }

    }

    /**
     * Create a response with $result_obj in the body
     * @param object|array $result_obj
     * @return Response
     */
    private function createResponse($result_obj)
    {
        $data = $result_obj;
        $view = $this->view($data);
        $response =  $this->handleView($view);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE');
        return $response;
    }

    /**
     * Create an empty response with 404 status code
     * @return Response
     */
    private function createNotFoundResponse()
    {
        $ret = $this->createResponse(null);
        $ret->setStatusCode(Response::HTTP_NOT_FOUND);
        return $ret;
    }

    /**
     * Create a response with the exception message in the body and 400 status code
     * @param \Exception $exception
     * @return Response
     */
    private function handleError($exception)
    {
        $ret = $this->createResponse(array("error" => $exception->getMessage()));
        $ret->setStatusCode(Response::HTTP_BAD_REQUEST);
        return $ret;
    }

}