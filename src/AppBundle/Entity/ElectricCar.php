<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 */
class ElectricCar extends BaseCar
{

    public function __construct()
    {
        parent::__construct();
        $this->carType = "Electric car";
    }

    public function isCompatible($task)
    {
        if($task->getMaintenanceType() == "Oil change")
        {
            return false;
        }
        return true;
    }
}