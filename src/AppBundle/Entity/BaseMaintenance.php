<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Table("maintenance_tasks")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"base" = "BaseMaintenance", "brake_refill" = "BrakeRefill", "tire_rotation" = "TireRotation", "oil_change" = "OilChange"})
 * @ORM\Entity
 */
class BaseMaintenance
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $date;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $cost;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $maintenanceType;

    /**
     * @var BaseCar
     * @ORM\ManyToOne(targetEntity="BaseCar", inversedBy="maintenance_tasks", cascade={"persist"})
     */
    protected $car;

    public function __construct()
    {
        $this->date = new \DateTime();
        $this->cost = 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return BaseMaintenance
     */
    public function setDate($date)
    {
        if($date){
            $this->date = $date;
        }
        return $this;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     * @return BaseMaintenance
     */
    public function setCost($cost)
    {
        if($cost >= 0)
            $this->cost = $cost;
        return $this;
    }

    /**
     * @return BaseCar
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * @param BaseCar $car
     * @return BaseMaintenance
     */
    public function setCar($car)
    {
        $this->car = $car;
        return $this;
    }

    /**
     * @return string
     */
    public function getMaintenanceType()
    {
        return $this->maintenanceType;
    }

    public function toArray()
    {
        $ret = array(
            "id" => $this->getId(),
            "car_id" => $this->car->getId(),
            "type" => $this->getMaintenanceType(),
            "date" => $this->getDate()->format("d-m-Y"),
            "cost" => $this->getCost()
        );
        return $ret;
    }

}