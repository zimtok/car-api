<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class DieselCar extends BaseCar
{
    public function __construct()
    {
        parent::__construct();
        $this->carType = "Diesel car";
    }
}