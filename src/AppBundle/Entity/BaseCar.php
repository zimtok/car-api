<?php


namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("cars")
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"base" = "BaseCar", "electric" = "ElectricCar", "gas" = "GasCar", "diesel" = "DieselCar"})
 */
class BaseCar
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $year;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $make;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $model;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $odometer;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $carType;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="BaseMaintenance", mappedBy="car", cascade={"all"})
     */
    protected $maintenance_tasks;

    /**
     * BaseCar constructor.
     */
    public function __construct()
    {
        $this->maintenance_tasks = new ArrayCollection();
        $this->year = 1900;
        $this->odometer = 0;
        $this->make = 'N/A';
        $this->model = 'N/A';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     * @return BaseCar
     */
    public function setYear($year)
    {
        if($year >= 1900)
            $this->year = $year;
        return $this;
    }

    /**
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * @param string $make
     * @return BaseCar
     */
    public function setMake($make)
    {
        if($make)
            $this->make = $make;
        return $this;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return BaseCar
     */
    public function setModel($model)
    {
        if($model)
            $this->model = $model;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMaintenanceTasks()
    {
        return $this->maintenance_tasks;
    }


    /**
     * @param BaseMaintenance $task
     */
    public function addMaintenanceTask($task)
    {
        if($this->isCompatible($task)) {
            $task->setCar($this);
            $this->maintenance_tasks->add($task);
        }else{
            throw new \UnexpectedValueException("This task cannot be performed on this type of car");
        }
    }


    public function removeMaintenanceTask($id)
    {

        foreach ($this->maintenance_tasks as $task){
            if($task->getId() == $id){
                $this->maintenance_tasks->removeElement($task);
            }
        }
    }

    /**
     * @param ArrayCollection $maintenance_tasks
     * @return BaseCar
     */
    public function setMaintenanceTasks($maintenance_tasks)
    {
        $this->maintenance_tasks = $maintenance_tasks;
        return $this;
    }

    /**
     * @return int
     */
    public function getOdometer()
    {
        return $this->odometer;
    }

    /**
     * @param int $odometer
     * @return BaseCar
     */
    public function setOdometer($odometer)
    {
        if($odometer >= 0)
            $this->odometer = $odometer;
        return $this;
    }

    /**
     * @return string
     */
    public function getCarType()
    {
        return $this->carType;
    }

    /**
     * @param BaseMaintenance $task
     * @return bool
     */
    public function isCompatible($task)
    {
        return true;
    }

    public function toArray()
    {
        $ret = array(
            "id" => $this->getId(),
            "type" => $this->getCarType(),
            "make" => $this->getMake(),
            "model" => $this->getModel(),
            "year" => $this->getYear(),
            "odo" => $this->getOdometer(),
            "maintenance_tasks" => array()
        );
        foreach ($this->getMaintenanceTasks() as $task){
            $ret["maintenance_tasks"][] = $task->toArray();
        }
        return $ret;

    }
}