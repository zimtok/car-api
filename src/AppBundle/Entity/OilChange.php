<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class OilChange extends BaseMaintenance
{
    public function __construct()
    {
        parent::__construct();
        $this->maintenanceType = "Oil change";
    }
}