<?php


namespace AppBundle\Entity;


use Symfony\Component\HttpFoundation\Request;

class CarFactory
{
    /**
     * @param Request $request
     * @return BaseCar
     */
    public static function createFromRequest($request)
    {
        $car = null;
        switch ($request->get('type')){
            case 'electric':
                $car = new ElectricCar();
                break;
            case 'gas':
                $car = new GasCar();
                break;
            case 'diesel':
                $car = new DieselCar();
                break;
            default:
                throw  new \UnexpectedValueException("Car type unspecified");
        }
        self::setParameters($car, $request);
        return $car;
    }

    /**
     * @param BaseCar $car
     * @param Request $request
     *
     * @return BaseCar
     */
    public static function setParameters($car, $request){
        $odo = intval($request->get('odo'));
        $year = intval($request->get('year'));
        $make = $request->get('make');
        $model = $request->get('model');
        $car->setOdometer($odo)
            ->setYear($year)
            ->setMake($make)
            ->setModel($model);
        return $car;
    }

}