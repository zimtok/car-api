<?php


namespace AppBundle\Entity;


use Symfony\Component\HttpFoundation\Request;


class MaintenanceFactory
{
    public static function createFromRequest(Request $request)
    {
        $task = null;
        switch ($request->get('type')){
            case 'oil_change':
                $task = new OilChange();
                break;
            case 'tire_rotation':
                $task = new TireRotation();
                break;
            case 'brake_refill':
                $task = new BrakeRefill();
                break;
            default:
                throw new \UnexpectedValueException("Task type unspecified");
        }
        self::setParameters($task, $request);
        return $task;

    }

    /**
     * @param BaseMaintenance $task
     * @param Request $request
     */
    public static function setParameters($task, $request)
    {
        $cost = floatval($request->get('cost'));
        $date = \DateTime::createFromFormat("d-m-Y", $request->get('date', ""));
        $task->setDate($date);
        $task->setCost($cost);

    }
}