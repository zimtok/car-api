Car Inventory API
=================

Description
---
This API makes it possible to manage car objects and to associate them 
with maintenance tasks. These objects can be retrieved, created, updated, 
and deleted through the API.

Car objects
----
The fields of a car object that can be supplied by the user are listed below. When a field is marked as not obligatory
its value will be the default if no value or an invalid value is supplied.

| Field name | Field type                                  | Obligatory          | Default |
|:-----------|:--------------------------------------------|:--------------------|:--------|
| type       | string(`electric`&#124;`gas`&#124;`diesel`) | yes                 | -       |
| make       | string                                      | no                  | "N/A"   |
| model      | string                                      | no                  | "N/A"   |
| year       | integer                                     | no                  | 1900    |

Example of a car object returned by the API:

```json
{
    "id": 7, 
    "maintenance_tasks": [], 
    "make": "Batmobile", 
    "model": "N/A", 
    "type": "Electric car", 
    "year": 1996
}
```

Maintenance task objects
---
The fields of a maintenance task object that can be supplied by the user are listed below. When a field is marked as not obligatory
its value will be the default if no value or an invalid value is supplied.

| Field name | Field type                                                   | Obligatory          | Default          |
|:-----------|:-------------------------------------------------------------|:--------------------|:-----------------|
| car_id     | integer (valid id of car object)                             | yes                 | -                |
| type       | string(`oil_change`&#124;`brake_refill`&#124;`tire_rotation`)| yes                 | -                |
| cost       | float                                                        | no                  | 0                |
| date       | date in "dd-mm-yyyy" format                                  | no                  | the current date |

Example of a maintenance task object returned by the API:

```json
{
    "car_id": 7, 
    "cost": 250, 
    "date": "01-04-2001", 
    "id": 18, 
    "maintenance_type": "Brake fluid refill"
}
```

API calls
----

| Description                | List all cars                          |
|:---------------------------|:---------------------------------------|
| URL                        | /api                                   |
| Method                     | GET                                    |
| Request parameters         | None                                   |
| Success Response Code      | 200                                    |
| Success Response Content   | list of car objects                    |

| Description             | Get data about one car                                 |
|:------------------------|:-------------------------------------------------------|
| URL                     | /api/{car-id}                                          |
| Method                  | GET                                                    |
| Request Parameters      | None                                                   |
| Success Response Code   | 200 Content: car object                                |
| Success Response Content| Car object                                             |
| Error Response Code     | 404                                                    |
| Error Response Content  | None                                                   |
| Error  cause            | No car with the specified ID                           |

| Description              | Create a new car                  |
|:-------------------------|:----------------------------------|
| URL                      | /api                              |
| Method                   | POST                              |
| Request fields           | type, year, make, model, odo      |
| Success response code    | 200                               |
| Success response content | New car object                    |
| Error response code      | 400                               |
| Error response content   | `"error": "Car type unspecified"` |
| Error cause              | The type parameter is obligatory  |

| Description                    | Edit or create a car              |
|:-------------------------------|:----------------------------------|
| URL                            | /api/{car-id}                     |
| Method                         | PUT                               |
| Request fields                 | type, year, make, model, odo      |
| Successful edit response code  | 200                               |
| Success response content       | Car object with updated fields    |
| Successful creation code       | 201                               |
| Successful creation content    | New car object                    |
| Error response code (creation) | 400                               |
| Error response content         | `"error": "Car type unspecified"` |
| Error cause                    | The type parameter is obligatory  |

| Description              | Delete a car                                |
|:-------------------------|:--------------------------------------------|
| URL                      | /api/{car-id}                               |
| Method                   | DELETE                                      |
| Request fields           | None                                        |
| Success response code    | 204                                         |
| Success response content | None                                        |
| Error response code      | 404                                         |
| Error response content   | None                                        |
| Error cause              | The car with the specified ID doesn't exist |

| Description              | List all tasks for a car                    |
|:-------------------------|:--------------------------------------------|
| URL                      | /api/{car-id}/task                          |
| Method                   | GET                                         |
| Request fields           | None                                        |
| Success response code    | 200                                         |
| Success response content | List of maintenance task objects            |
| Error response code      | 404                                         |
| Error response content   | None                                        |
| Error cause              | The car with the specified ID doesn't exist |

| Description              | Get one task for a car                                                                 |
|:-------------------------|:---------------------------------------------------------------------------------------|
| URL                      | /api/{car-id]/task/{task-id}                                                           |
| Method                   | GET                                                                                    |
| Request fields           | None                                                                                   |
| Success response code    | 200                                                                                    |
| Success response content | Maintenance task objects                                                               |
| Error response code      | 404                                                                                    |
| Error response content   | None                                                                                   |
| Error cause              | The car or task with the specified ID doesn't exist or the task belongs to another car |

| Description              | Create a new maintenance task               |
|:-------------------------|:--------------------------------------------|
| URL                      | /api/{car-id]/task                          |
| Method                   | POST                                        |
| Request fields           | type, cost, date                            |
| Success response code    | 200                                         |
| Success response content | The new maintenance task object             |
| Error response code      | 404                                         |
| Error response content   | None                                        |
| Error cause              | The car with the specified ID doesn't exist |
| Error response code      | 400                                         |
| Error response content   | `"error": "Car type unspecified"`           |
| Error cause              | The type parameter is obligatory            |
| Error response code      | 400                                         |
| Error response content   | `"error": "This task cannot be performed on this type of car"`           |
| Error cause              | The specified task cannot be performed on the type of the target car          |

| Description                         | Create or edit a maintenance task               |
|:------------------------------------|:------------------------------------------------|
| URL                                 | /api/{car-id]/task/{task-id}                    |
| Method                              | PUT                                             |
| Request fields                      | type, cost, date                                |
| Success response code (edit)        | 200                                             |
| Success response content (edit)     | The maintenance task object with updated fields |
| Success response code (creation)    | 201                                             |
| Success response content (creation) | The new maintenance task object                 |
| Error response code                 | 404                                             |
| Error response content              | None                                            |
| Error cause                         | The car with the specified ID doesn't exist     |
| Error response code (creation)      | 400                                             |
| Error response content              | `"error": "Car type unspecified"`               |
| Error cause                         | The type parameter is obligatory                |
| Error response code                 | 400                                             |
| Error response content              | `"error": "This task cannot be performed on this type of car"`                |
| Error cause                         | The specified task cannot be performed on the type of the target car          |

| Description              | Delete a maintenance task                    |
|:-------------------------|:---------------------------------------------|
| URL                      | /api/task/{task-id}                          |
| Method                   | DELETE                                       |
| Request fields           | None                                         |
| Success response code    | 204                                          |
| Success response content | None                                         |
| Error response code      | 404                                          |
| Error response content   | None                                         |
| Error cause              | The task with the specified ID doesn't exist |
